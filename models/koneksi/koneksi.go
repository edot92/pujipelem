package koneksi
import (
	"fmt"
	"log"

	lumberjack "gopkg.in/natefinch/lumberjack.v2"

	"strconv"
	"time"

	"github.com/astaxie/beego"
	"github.com/jinzhu/gorm"
)

func init() {

	log.SetOutput(&lumberjack.Logger{
		Filename:   "error.log",
		MaxSize:    10, // megabytes
		MaxBackups: 100,
		MaxAge:     1, //days
	})
}
func CobaKonek() (*gorm.DB, error) {
	// Env, errRead := config.ReadDefault("pengaturan.env")
	var db *gorm.DB
	var err error
	beego.AppConfig.String("TYPE_DATABASE")
	typeDatabase := beego.AppConfig.String("TYPE_DATABASE")
	if typeDatabase == "mysql" {
		user := beego.AppConfig.String("DB_USERNAME")
		pass := beego.AppConfig.String("DB_PASSWORD")
		database := beego.AppConfig.String("DB_DATABASE")
		host := beego.AppConfig.String("DB_HOST")
		port := beego.AppConfig.String("DB_PORT")
		db, err = gorm.Open("mysql", user+":"+pass+"@tcp("+host+":"+port+")/"+database+"?charset=utf8&parseTime=True&loc=Local&writeTimeout=5s&readTimeout=5s&timeout=5s")
	} else if typeDatabase == "sqlite3" {
		database := beego.AppConfig.String("DB_DATABASE")
		db, err = gorm.Open("sqlite3", database+".db")
	}
	// db, err := gorm.Open("mysql", "root:@/gatewaydb")
	if err != nil {
		// log.Fatal(err)
		panic(err)

	} else {
		log.Println("terkoneksi dengan database")

	}
	db.LogMode(false)
}