package routers

import (
	"github.com/astaxie/beego"
	"gitlab.com/edot92/pujipelem/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{}, "get:Index")
}
