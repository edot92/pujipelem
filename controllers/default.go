package controllers

import (
	"bytes"

	template "gitlab.com/edot92/pujipelem/template/fujifilm"

	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

// MainController.Index
func (c *MainController) Index() {
	a := "hehehe"

	buffer := new(bytes.Buffer)
	template.Rs(a, buffer)

	c.Ctx.ResponseWriter.Write(buffer.Bytes())
	c.Ctx.Output.Header("text/html", "charset=utf-8")
}
