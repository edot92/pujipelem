package main

import (
	"github.com/astaxie/beego"
	_ "gitlab.com/edot92/pujipelem/routers"
)

func main() {
	// if beego.BConfig.RunMode == "dev" {
	// beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	// }
	beego.BConfig.WebConfig.DirectoryIndex = true
	// beego.BConfig.WebConfig.StaticDir["/assets"] = "assets"
	beego.BConfig.WebConfig.ViewsPath = "views"
	beego.BConfig.WebConfig.TemplateLeft = "<<<"
	beego.BConfig.WebConfig.TemplateRight = ">>>"
	beego.SetStaticPath("/assets", "assets")

	beego.Run()
}
