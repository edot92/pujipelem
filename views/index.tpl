
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>RSHS - RESERVASI ONLINE - Langkah 1 - Verifikasi Identitas Pasien</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="Reservasi Online, memfasilitasi pendaftaran pelayanan rawat jalan melalui pendaftaran online.">
        <meta name="author" content="SIRS (Instalasi Sistem Informasi Rumah Sakit) - RSUP Dr. Hasan Sadikin Bandung">
        <meta name="keywords" content="Reservasi Online RSHS, Reservasi, Pelayanan Rawat Jalan, Contact Center">
        <meta name="theme-color" content="#4C8AFF">
        <meta name="msapplication-navbutton-color" content="#4C8AFF">
        <meta name="apple-mobile-web-app-status-bar-style" content="#4C8AFF">

        <link rel="shortcut icon" href="http://reservasi.rshs.or.id/favicon.ico" type="image/x-icon"/>
        <link rel="alternate" href="http://reservasi.rshs.or.id/" hreflang="id" />

        <link rel="stylesheet" href="http://reservasi.rshs.or.id/assets/css/component/bootstrap.css">
        <link rel="stylesheet" href="http://reservasi.rshs.or.id/assets/css/component/choosen/chosen.css" >
        <link rel="stylesheet" href="http://reservasi.rshs.or.id/assets/css/component/datepicker.css" >
        <link rel="stylesheet" href="http://reservasi.rshs.or.id/assets/css/component/select2.min.css" >
        <link rel="stylesheet" href="http://reservasi.rshs.or.id/assets/css/component/jquery-toast.css" >
        <!-- <link rel="stylesheet" href="http://reservasi.rshs.or.id/assets/css/component/jquery-ui-boo.css" > -->
        <link rel="stylesheet" href="http://reservasi.rshs.or.id/assets/css/component/progress-wizard.css" >
        <link rel="stylesheet" href="http://reservasi.rshs.or.id/assets/css/default.css" >

        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/jquery.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/jquery-ui.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/bootstrap.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/jquery-tablesorter.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/jquery-tablecomponent.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/jquery-inputmask-bundle.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/jquery-select2.min.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/jquery-autotab.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/jquery-validate.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/jquery-push_notification.js"></script>

        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/graphics/dx.chartjs.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/graphics/globalize.min.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/graphics/knockout-2.2.1.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/peity.js"></script>

        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/timeago.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/component/jquery-toast.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/ajax_load.js"></script>
        <script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/ga.js"></script>
    </head>  <body>
    <noscript>
      <div class="alert alert-danger">
        <strong>Error..!!</strong>
        <span></span>
      </div>
    </noscript>

    <div id='loading-overlay-layer' class='hidden-print'>
      <div class="progress progress-striped progress-bar-warning active">
        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
          <span class="sr-only">Loading</span>
        </div>
        <img src='http://reservasi.rshs.or.id/assets//img/loader/ajax-loader.gif'/>
      </div>
    </div>

    <div class='overlay hidden-print'>
      <div class="alert alert-danger" id="error_ajax">
        <strong>Error..!!</strong>
        <span></span>
        <div class="tutup_error"><a href="javascript:void(0);"><span class="glyphicon glyphicon-circle-arrow-left"></span> Kembali</a></div>
      </div>
    </div>

    <div id='wrap'>
      <div class="hidden navbar navbar-default hidden-print">
          <div class="navbar-header">
            <a href="" class="navbar-brand"><img width='60px' src='http://reservasi.rshs.or.id/assets/img/logo/logo.png'></a>
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li>
                <a href="http://reservasi.rshs.or.id/">
                  <span class='glyphicon glyphicon-home'></span> DASHBOARD
                </a>
              </li>
            </ul>
          </div>
      </div>
  <div class="docs-header hidden-print hidden-md hidden-sm hidden-xs">
    <div class="container">
      <div class='col-lg-7'>
	<h3> Reservasi Online <sup style="background-color: red; font-size: 10px;padding: 2px;">Beta</sup><br>RSUP dr.Hasan Sadikin - Bandung</h3>
	<p>Reservasi Online, memfasilitasi pendaftaran pelayanan rawat jalan melalui pendaftaran online.<br>Pendaftaran Online dapat dilakukan minimal 2 hari sebelum rencana reservasi dilakukan.<br>Pasien akan mendapatkan SMS Reservasi yang memudahkan saat daftar ulang pada pelayanan rawat jalan</p>
</div>
<div class='col-lg-5 hidden-md hidden-sm hidden-xs'>
	<br>
	<div class='well black-text font-14 capitalize-font'>
		<ul class="progress-indicator">
			<li class='completed'><span class="bubble"></span> Langkah 1. <br>Verifikasi Identitas Pasien</li>
			<li class='uncompleted'><span class="bubble"></span> Langkah 2. <br>Pilih Jadwal Reservasi</li>
			<li class='uncompleted'><span class="bubble"></span> Langkah 3. <br>Resume Jadwal Reservasi</li>
		</ul>
	</div>
</div>    </div>
  </div>

<div id='main_container' class='container'>



    <div class='col-lg-7 col-md-7 col-sm-8 visible-lg visible-md'>
	<div class='content_informasi android'></div>
	<div class='content_text'>
		Download Gratis Aplikasi RSHS GO Mobile<br>
		Untuk Handphone Android Anda.<br>
		<hr>
		Untuk Informasi Lebih Lanjut<br>
		Silahkan Hubungi Contact Center Kami di <b>022 - 255 1111</b><br>
		Kami Siap Melayani Anda.<br>
		<!-- 172.68.146.23 -->
	</div>
</div>
<div class='col-lg-5 col-md-5 col-sm-12'>
	<div id='main_container2' class='panel panel-default panel-shadow panel-background '>
		<div class='panel-body'>
			<div class='pre-header-image visible-xs visible-sm'></div>
			<div class='ribbon'><strong class="ribbon-content"> LANGKAH 1 - VERIFIKASI IDENTITAS PASIEN</strong></div>
			<div class='info_container'>
				<h5>
					<center>
						<small>
							Pastikan anda sudah terdaftar sebagai pasien <br>
							RSUP Dr. Hasan Sadikin Bandung.<br><br>
							<span class='visible-lg'>
								Masukan No.Rekam Medik & Tanggal lahir <br>
								sesuai dengan yang tertera di KIP (Kartu Indentitas Pasien) <br>
								yang diterima ketika mendaftar
							</span>
						</small>
					</center>
				</h5>
				<hr>
				<form class='get_ajax_submit' method='POST' target_container='.result_identitas' action='http://reservasi.rshs.or.id/reservasi/identitas_pasien'>
					<div class="form-group">
						<label>Masukan No.Rekam Medik</label>
						<input name='q' autocomplete="off" class='medrec center form-control text_masking' maxlength='10' autofocus></input>
					</div>
					<div class="form-group">
						<label>Masukan Tanggal Lahir</label>
						<input name='l' autocomplete="off" class='tanggal center form-control text_masking' data-inputmask="'alias': 'date'"></input>
					</div>
					<div class='result_identitas'>
						<button type="submit" id='btn_cari_data' class="form-control btn btn-primary">CARI DATA PASIEN</button>
						<div class='hidden-sm hidden-xs'>
							<hr class='dash'/>
							<h5>
								<center>
									<small>Petunjuk Letak Informasi Identitas Pasien berdasarkan KIP (Kartu Identitas Pasien) RSUP Dr. Hasan Sadikin Bandung, untuk dinputkan pada form diatas</small>
								</center>
							</h5>
							<img src='http://reservasi.rshs.or.id/assets//img/component/kip.png' class="img-responsive"/>
						</div>
					</div>
				</form>
			</div>
			<div class='info_wizard hidden-lg hidden-print'>
				<hr class='dash'>
				<ul class="progress-indicator">
					<li class='completed'><span class="bubble"></span> Langkah 1. <br>Verifikasi Identitas Pasien</li>
					<li class='uncompleted'><span class="bubble"></span> Langkah 2. <br>Pilih Jadwal Reservasi</li>
					<li class='uncompleted'><span class="bubble"></span> Langkah 3. <br>Resume Jadwal Reservasi</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/modules/index/index.js"></script><link rel="stylesheet" href="http://reservasi.rshs.or.id/assets/css/modules/index/index.css" /><script type="text/javascript">
	$('.medrec').autotab({ format: 'numeric', target: '.tanggal' });
</script>          <div class='prefooter visible-sm visible-xs'>
            <span>
              RESERVASI ONLINE - Data Terpadu SIRS <br>RSUP Dr. Hasan Sadikin Bandung<br> &copy; SIRS - 2017            </span>
          </div>
        </div>
    </div>

    <div id='loading_panel' class='hidden-print'>
      <div id='loading-panel-container'>
        <img src='http://reservasi.rshs.or.id/assets//img/loader/ajax-loader.gif'/> <a class='cancel_ajax btn btn-link'> Cancel</a>
      </div>
    </div>

    <div class="modal fade hidden-print" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
        </div>
      </div>
    </div>

    <div class='scroll_top hidden-print'>
      <a id='to-top' href='javascript:void(0);'><span class='glyphicon glyphicon-chevron-up'></span></a>
    </div>


    <div class='running_text'></div>
    <div id="footer" class='hidden-print hidden-md hidden-sm hidden-xs'>
      <div class="container">
        <div class='pull-left'>
          <p class="credit"><img height='46px;' src='http://reservasi.rshs.or.id/assets/img/logo/sirs_logo_transparent-xs.png' style='margin-right: 10px;'/></p>
        </div>
        <div></div>
        <div class="pull-right">
          <p class="credit"><img height='46px;' src="http://reservasi.rshs.or.id/assets/img/logo/logo_title.png"><img width='70px;' src='http://reservasi.rshs.or.id/assets//img/component/ribbon_jci.png' style='margin-right: 10px;'/></p>
        </div>
        <p class="credit hidden-xs hidden-sm">
          RESERVASI ONLINE - Data Terpadu SIRS <br>RSUP Dr. Hasan Sadikin Bandung<br> &copy; SIRS - 2017      </div>
    </div>
    <div id='loading_custom'></div>
    <div id='loading_custom_detail'></div>
  <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "cfs.uzone.id/2fn7a2/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582CL4NjpNgssKSSIKbjcjkIXrt%2bOsZVuacx9W%2bVGB0RwjsxloRiLnR7fCl5jDOOvgwSlXDoayhd6DoHJ047zYQedrFd3H4qRbXVhZ%2ffTW%2fuvYUBp%2fyZqN1ELDXwDZLT7f0V6WAWM1dxNPLLecdUQ2HoQN4JiQuUunVphLL0DhhU%2ffbGnaqG5BGBiH2T1LJuoUARWrCiMyKDaMSY0nKNxd3kuW8yNqdRnW5LhyiPzZdz636371B1dievVwDheu2o%2bcaj7cWCCljLqw%2fTpZIcXODcBokYDBvij9cS9sy%2baRNOgHB9NxHxSolG2FUWdCY4v2CDL3W8Nct%2brdPQzm9OXshBda7g8fJeD%2bkninEHV73LtX70lfUdd3i995mhhWgsZ6Sd6W1PYVj9Mm84V0jm%2f%2fMdChBIAB4s4ghI%2bxti%2bQ6x6ftTQ%2fIgT3vuPYCNBSUjMvcA41EFfUM7wWvqavSqqicMb5cigyV%2fbarDAcAbTRdxEUfnRfZS%2bxamwff%2bvCKuCeV22rKwO9pJylWBZlJ3Lj4xxNpqGG03W8toupc%2f7iDiQr" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>

<script type="text/javascript" src="http://reservasi.rshs.or.id/assets/js/default.js"></script>
